#!/usr/bin/env bash

IP="192.168.4.10"; # Address web server.

SQL_PASSWORD_ROOT='pei_W0oo'; # Password for user root within MariaDB.
SQL_PASSWORD_USER='ota0Oob%'; # Password for new users within MariaDB.

WWW_USER_GROUP="www-data.www-data"; # This's owner directories sites.

DEFAULT_BIND9="/etc/default/bind9";
DIR_BIND='/etc/bind';
DIR_ZONES="$DIR_BIND/zones";
FILE_NAMED_CONF="$DIR_BIND/named.conf";
FILE_NAMED_OPTS="$DIR_BIND/named.conf.options";
FILE_NAMED_ZONE="$DIR_BIND/named.conf.zones";

DIR_APACHE2='/etc/apache2';
DIR_SITES_AVAILABLE="$DIR_APACHE2/sites-available";
DIR_SITES_ENABLED="$DIR_APACHE2/sites-enabled";
DOCUMENT_ROOT="/var/www/html";
