#!/usr/bin/env bash

LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# Exit if error.
die() {
    if [[ -n "$1" ]]; then
        echo -e "Error: $1\n" >&2;
        exit;
    else
        echo -e "Error undefined.\n" >&2;
        exit;
    fi
}


# check substring into file.
function is_exist {
    local substr=$1;
    local path=$2;

    [[ ! -z $(sed -n "/${substr//\//\\/}/p" $path;) ]]
}

# Add to file entry with include path to file zones "named.conf.zones"
function edit_named_conf {
    local named_conf=$1;
    local named_zone=$2;

    # If string isn't exist, then will be add.
    if is_exist "$named_zone" "$named_conf"; then
        echo "Entry with \"$named_zone\" into \"$named_conf\" exist.";
    else
        echo "Entry with \"$named_zone\" into \"$named_conf\" not exist, will be add.";
        echo "include \"$named_zone\";" >> "$named_conf" || die "can\'t change file \"$named_conf\".";
    fi
}

# Create file with zones of domains.
function create_file_zones {
    local named_zone=$1;

    # If file isn't exist, then create.
    if [[ -f $named_zone ]]; then
        echo "File \"$named_zone\" exist.";
    else
        echo "File \"$named_zone\" not exist, will be create.";
        touch "$named_zone" || die "can\'t create file \"$named_zone\".";
    fi
}

# Disable IPv6 for bind9
function change_default_bind9 {
    local path=$1;
    substr='-u bind -4';

    # If file exist and option not set, then will be edit.
    if [[ -f $path ]]; then
        if is_exist "${substr}" "$path"; then
            echo "Substring \"$substr\" into \"$path\" exist";
        else
            echo "Substring \"$substr\" into \"$path\" not exist, will be change.";
            sed -i "s/^OPTIONS=.*/OPTIONS=\"$substr\"/" "$path" || die "can\'t change file \"$path\".";
        fi
    else
        echo "File \"$path\" not exist!!!";
    fi
}

# Create directory for files with  domain's zone.
function create_dir_zones {
    local dir_zones="$1";

    # If directory isn't exist, then will be create.
    if [[ -d "$dir_zones" ]]; then
        echo "Directory \"$dir_zones\" exist.";
    else
        echo "Directory \"$dir_zones\" not exist, will be create.";
        mkdir -p "$dir_zones" || die "can\'t create dir \"$dir_zones\"."
    fi
}

# Check is enabled apache's module for php.
function enable_php {
    local vers="$1";

    # If disable, then will be enable.
    if a2query -qm php$vers ; then
        echo "Module \"php$vers\" enable.";
    else
        echo "Module \"php$vers\" disable, will be enable.";
        a2enmod "php$vers" || die "can\'t enabled \"php$ver\"."
    fi
}

# Set password for user "root" of MariaDB
function set_root_pass {
    local pass=$1

    mysqladmin -u root password $pass || die "can\'t set password for root from MariaDB."
}

# Install packages
apt update || die "can\'t updated apt cache."
apt -y install bind9 apache2 php mariadb-server || die "apt can\'t installed packages."

# Run preconfigure
edit_named_conf "$FILE_NAMED_CONF" "$FILE_NAMED_ZONE";
create_file_zones "$FILE_NAMED_ZONE";
change_default_bind9 "$DEFAULT_BIND9";
create_dir_zones "$DIR_ZONES";
enable_php `ls /etc/php`; # Get version PHP, but maybe better use: $(ls `a2query -d` |grep php |sed 's/libphp//;s/\.so//')
set_root_pass "$SQL_PASSWORD_ROOT";

