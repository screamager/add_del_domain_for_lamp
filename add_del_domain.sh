#!/usr/bin/env bash

LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

ARGS=("$@")
LIST_OPTS=(
    'add'   # Add domain
    'del'   # Delete domain
    '-d'    # Domain name
    '-a'    # Alias domain, maybe a few
    '-h'    # Help
)

# Help using.
help() {
    echo -e "Usage:\t$0: <add | del> -d <domain name> [[-a <alias domain>] ...]" >&2;
}

# Exit if error.
die() {
    if [[ -n "$1" ]]; then
        echo -e "Error: $1\n" >&2;
        exit;
    else
        echo -e "Error undefined.\n" >&2;
        exit;
    fi
}

# Check of the validate arguments
function check_args () {
    local -n local_args=$1;
    local -n local_opts=$2;

    # Arguments not should be less three.
    if [ ${#local_args[@]} -lt 3 ]; then
        # If first argument is "-h" show help and exit.
        if [ "_${local_args[0]}" == "_${local_opts[4]}" ] ; then
            help; exit;
        fi
        help; die "Too few arguments.";
    fi
    # First argement expect as "add" or "del".
    if [[ "_${local_args[0]}" != "_${local_opts[0]}" ]] && [[ "_${local_args[0]}" != "_${local_opts[1]}" ]]; then
        help; die "Bad first argument (${local_args[0]}).";
    fi
    # Second argument expect as "-d" and next should be domain name.
    if [[ "_${local_args[1]}" != "_${local_opts[2]}" ]]; then
        help; die "Bad second argument (${local_args[0]}).";
    fi
    # Next pairs should be as "-a <alias domain name>".
    for i in `seq 3 "$((${#local_args[@]} - 4))"`; do
        if [[ $(($i % 2)) -ne 0 ]] ; then
            if [[ "_${local_args[$i]}" != "_${local_opts[3]}" ]]; then
                help; die "There is bad argument.";
            fi
        fi
    done
}

######### BIND SERVER ###############
## Functions add domain.
# Add entry into the file "named.conf.zones".
function add_to_named_conf_zones () {
    local domain_name=$1;
    local file_zones=$2;

    # If entry isn't, then will be add.
    if [[ -z `sed -n "/$domain_name/,/}/p" $file_zones` ]]; then
        echo "Domain: $domain_name is not exist, will be add.";
        echo -e "\nzone \"$domain_name\" {\n\ttype master;\n\tfile \
\"${DIR_BIND}/zones/db.$domain_name\";\n};" >> $file_zones || die "can\'t change file \"$file_zone\"."
        # Replace empty lines to one empty line.
        sed -i ':a;/^$/N;/\n$/{D;ba}' $file_zones;
    else
        echo "Domain: \"$domain_name\" is exist.";
    fi
}

# Create file domain zone.
function add_file_domain_zone () {
    local ip=$1
    local domain_name=$2;
    local file_zone=$3;
    local -n local_aliases=$4;
    local local_date=`date +'%Y%m%d'`;

    # If is not exist then create.
    if [[ -f $file_zone ]]; then
        echo "File \"$file_zone\" is exist";
    else
        echo "File \"$file_zone\" isn't exist, will be create.";
        cat << EOF > $file_zone || die "can\'t create file \"$file_zone\".";
\$TTL 15m
@   IN  SOA ns.${domain_name}.   root.${domain_name}. (
         ${local_date}01     ; Change every time when edit file, format YYMMDDNN, NN - sequence number
                 4h     ; Refresh
                 1h     ; Retry
                 2w     ; Expire
                15m )   ; Negative Cache TTL

                IN  NS      ns.${domain_name}.
@               IN  A       $ip
ns              IN  A       $ip

EOF
        # Add lines with aliases domein.
        for i in `seq 0 "$((${#local_aliases[@]} - 1))"`; do
            echo "${local_aliases[$i]}. IN A $ip" >> $file_zone;
        done

    fi
}

## Functions delete domain.
# Delete entry from the file "named.conf.zones".
function del_to_named_conf_zones () {
    local domain_name=$1;
    local file_zones=$2;

    # If entry is exist, then will be delete.
    if [[ -z `sed -n "/$domain_name/,/}/p" $file_zones` ]]; then
        echo "Domain: $domain_name is not exist.";
    else
        echo "Domain: \"$domain_name\" is exist, will be delete";
        sed -i "/$domain_name/,/}/d" "$file_zones" || die "can\'t change file \"$file_zones\".";
        # Replace empty lines to one empty line.
        sed -i ':a;/^$/N;/\n$/{D;ba}' $file_zones;
    fi
}

# Delete file domain zone.
function del_file_domain_zone () {
    local domain_name=$1;
    local file_zone=$2;

    # If file is exist, then will be delete.
    if [[ -f "$file_zone" ]]; then
        echo "File \"$file_zone\" is exist, will be delete";
        rm -f "$file_zone" || die "can\'t remote file \"$file_zone\".";
    else
        echo "File \"$file_zone\" is not exist.";
    fi
}

######### APACHE2 SERVER ############
## Functions add domain.
# Create file with virtualhost
function add_virthost () {
    local domain_name=$1;
    local -n local_aliases=$2;
    local dir_sites=$3;
    local doc_root=$4

    # If file isn't exist, then will be create.
    if [[ -f "$dir_sites/${domain_name}.conf" ]]; then
        echo "File \"$dir_sites/${domain_name}.conf\" is exist";
    else
        echo "File \"$dir_sites/${domain_name}.conf\" isn't exist, will be create.";
        cat << EOF > $dir_sites/${domain_name}.conf || die "can\'t create file \"$dir_sites/${domain_name}.conf\".";
<VirtualHost *:80>
    ServerName $domain_name
    ServerAlias ${local_aliases[@]}

    ServerAdmin webmaster@localhost
    DocumentRoot $doc_root/$domain_name

    ErrorLog \${APACHE_LOG_DIR}/${domain_name}-error.log
    CustomLog \${APACHE_LOG_DIR}/${domain_name}-access.log combined

</VirtualHost>
EOF
    fi
    # Create symlink for enable site.
    if [[ ! -f "$dir_sites/../sites-enabled/${domain_name}.conf" ]]; then
        echo "Create \"sites-enabled/${domain_name}.conf\" symlink for enable"
        cd "$dir_sites/../sites-enabled";
        ln -sf "../sites-available/${domain_name}.conf" "${domain_name}.conf" \
            || die "can\'t create link to site-enabled from \"$dir_sites/${domain_name}.conf\".";
    fi
}

# Create dir for site of domain.
function add_doc_root {
    local domain_name="$1";
    local doc_root="$2";
    local user_group=$3;

    # If directory isn't exist, then create.
    if [[ -d "$doc_root/$domain_name" ]]; then
        echo "Directory \"$doc_root/$domain_name\" is exist.";
    else
        echo "Directory \"$doc_root/$domain_name\" is not exist, will be create.";
        mkdir -p "$doc_root/$domain_name" || die "can\'t create dir \"$doc_root/$domain_name\".";
    fi
    # Create test page with "phpinfo()"
    if [[ -f "$doc_root/$domain_name/index.php" ]]; then
        echo "File \"$doc_root/$domain_name/index.php\" is exist.";
    else
        echo "File \"$doc_root/$domain_name/index.php\" is not exist, will be create.";
        echo -e "<?php\n\tphpinfo()\n?>" > "$doc_root/$domain_name/index.php" \
            || die "can\'t create file \"$doc_root/$domain_name/index.php\".";
    fi
    # Set owner to directory site.
    chown -R $user_group "$doc_root/$domain_name" || die "can\'t change owners to dir \"$doc_root/$domain_name\".";
}

## Functions delete domain.
function del_virthost () {
    local domain_name=$1;
    local dir_sites=$2;

    # If file is exist, then will be delete.
    if [[ -f "$dir_sites/${domain_name}.conf" ]]; then
        echo "File \"$dir_sites/${domain_name}.conf\" is exist, will be delete";
        rm -f "$dir_sites/${domain_name}.conf" || die "can\'t delete file \"$dir_sites/${domain_name}.conf\".";
    else
        echo "File \"$dir_sites/${domain_name}.conf\" is not exist.";
    fi
    # Delete symlink from "sites-enabled".
    if [[ -L "$dir_sites/../sites-enabled/${domain_name}.conf" ]]; then
        echo "Symlink \"sites-enabled/${domain_name}.conf\" is exist, delete.";
        rm -f "$dir_sites/../sites-enabled/${domain_name}.conf" || die "can\'t delete \"sites-enabled/${domain_name}.conf\".";
    fi
}

# Delete dir with site of domain.
function del_doc_root {
    local domain_name="$1";
    local doc_root="$2";

    # If directory is exist, then recursive delete.
    if [[ -d "$doc_root/$domain_name" ]]; then
        echo "Directory \"$doc_root/$domain_name\" is exist, will be delete.";
        rm -fR "$doc_root/$domain_name" || die "can\'t delete dir \"$doc_root/$domain_name\".";
    else
        echo "Directory \"$doc_root/$domain_name\" is not exist.";
    fi
}

######### MARIADB SERVER ############
## Functions add domain.
# Create database and user with all privileges for domain.
function add_db_and_user {
    local dbname=${1//\./\_};
    local pass_root="$2";
    local pass_user="$3";

    # If dbase isn't exist, then create and add user
    if mysql -u root -p"$pass_root" "$dbname" -e 'exit' > /dev/null 2>&1; then
        echo "Databes \"$dbname\" is exist.";
        if [ `mysql -u root -p"$pass_root" -se "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = \"$dbname\")"` -eq 1 ]; then
            echo "User \"$dbname\" is exist.";
        else
            # If dbase is exist, then add user for it.
            echo "User \"$dbname\" is not exist, will be create.";
            mysql -u root -p"$pass_root" -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER
                ON $dbname.*
                TO '$dbname'@'localhost' IDENTIFIED BY '$pass_user';" \
                    || die "can\'t create mariadb user \"$dbname\".";
        fi
    else
        echo "Databes \"$dbname\" is not exist, will be create.";
        mysqladmin -u root -p"$pass_root" create $dbname || die "can\'t create mariadb database \"$dbname\".";
        mysql -u root -p"$pass_root" -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER
            ON $dbname.*
            TO '$dbname'@'localhost' IDENTIFIED BY '$pass_user';" \
                || die "can\'t create mariadb user \"$dbname\".";
    fi
}

## Functions delete domain.
# Delete database and user for it from domain.
function del_db_and_user {
    local dbname=${1//\./\_};
    local pass_root="$2";

    # If dbase is exist, then delete.
    if mysql -u root -p"$pass_root" "$dbname" -e 'exit' > /dev/null 2>&1; then
        echo "Database \"$dbname\" is exist, will be delete.";
        yes | mysqladmin -u root -p"$pass_root" drop $dbname || die "can\'t drop mariadb database \"$dbname\".";
    else
        echo "Databes \"$dbname\" is not exist.";
    fi

    if [ `mysql -u root -p"$pass_root" -se "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = \"$dbname\")"` -eq 1 ]; then
        echo "User \"$dbname\" is exist, will be delete.";
        mysql -u root -p"$pass_root" -e "DROP USER '$dbname'@'localhost';" || die "can\'t drop mariadb user \"$dbname\".";
    else
        echo "User \"$dbname\" is not exist.";
    fi
}

# Check service
function check_serv {
    local name_serv="$1"

    systemctl is-active --quiet $name_serv.service
}

# Reload service
function reload_serv {
    local name_serv="$1"

    systemctl reload --quiet $name_serv.service
}

# Restart service
function restart_serv {
    local name_serv="$1"

    systemctl restart --quiet $name_serv.service
}


# Check of the validate arguments.
check_args ARGS LIST_OPTS

# Get name of domain.
domain_name=${ARGS[2]}

# Shift arguments of script
shift 3
# Get aliases of domein as array.
unset alias_domain
declare -a alias_domain
while getopts "a:" opt; do
  case $opt in
    \?) die "Bad argument.";;
    a) alias_domain+=("$OPTARG");;
  esac
done

# Run add domain.
if [[ ${ARGS[0]} == "add" ]]; then
    # Bind9
    add_to_named_conf_zones $domain_name "$FILE_NAMED_ZONE"
    add_file_domain_zone $IP $domain_name "$DIR_ZONES/db.$domain_name" alias_domain
    # Apache2
    add_virthost $domain_name alias_domain $DIR_SITES_AVAILABLE $DOCUMENT_ROOT
    add_doc_root $domain_name $DOCUMENT_ROOT $WWW_USER_GROUP
    # MariaDB
    add_db_and_user $domain_name $SQL_PASSWORD_ROOT $SQL_PASSWORD_USER
fi
# Run delete domain.
if [[ ${ARGS[0]} == "del" ]]; then
    # Bind9
    del_to_named_conf_zones $domain_name "$FILE_NAMED_ZONE"
    del_file_domain_zone $domain_name "$DIR_ZONES/db.$domain_name"
    # Apache2
    del_virthost $domain_name $DIR_SITES_AVAILABLE
    del_doc_root $domain_name $DOCUMENT_ROOT
    # MariaDB
    del_db_and_user $domain_name $SQL_PASSWORD_ROOT
fi

# Check service and reload or start service
SERV="bind9"; check_serv $SERV && reload_serv $SERV || restart_serv $SERV || die "can\'t start service \"$SERV\".";
SERV="apache2"; check_serv $SERV && reload_serv $SERV || restart_serv $SERV || die "can\'t start service \"$SERV\".";
SERV="mariadb"; check_serv $SERV || restart_serv $SERV || die "can\'t start service \"$SERV\".";
