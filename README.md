# add_del_domain_for_LAMP

Add or delete domain to Bind9, Apache2 and MariaDB(Mysql).

This is tested only on Debian 9.7.

Use:

Run on server:
```
$ git clone https://gitlab.com/screamager/add_del_domain_for_lamp.git
$ vi add_del_domain_for_lamp/vars.sh # Set current IP and passwords.
$ sudo add_del_domain_for_LAMP/prepare_services.sh # Install and preconfig: bind9, apache2, php, mariadb
$ add_del_domain_for_LAMP/add_del_domain.sh -h # Show help for uses.
$ sudo add_del_domain_for_LAMP/add_del_domain.sh add -d example.net -a www.example.net -a webmail.example.net
```

Run on client:
```
$ sudo su -c 'echo "nameserver <IP your server>" >> /etc/resolv.conf
$ curl -s example.net | grep title # Should be page with phpinfo
$ curl -s www.example.net | grep title # Test alias domain
$ curl -s webmail.example.net | grep title
```

Delete domain on server:
```
$ sudo add_del_domain_for_LAMP/add_del_domain.sh del -d example.net
```
